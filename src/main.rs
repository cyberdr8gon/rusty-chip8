#[macro_use]

use std::time::Duration;
use std::env;
use std::io::Read;
use std::fs::File;

extern crate sdl2;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;



mod chip8;
use chip8::Chip8;

const FRAMES_PER_SECOND: u32 = 60;
const CLOCK_SPEED: u32 = 540;


fn main() {
    let args: Vec<_> = env::args().collect();
        if args.len() == 1 {
            println!("Error, no rom file specified.");
            return
    }

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
 
    let window = video_subsystem.window("rust-sdl2 demo", 800, 600)
        .position_centered()
        .build()
        .unwrap();
 
    let mut canvas = window.into_canvas().build().unwrap();

    let mut file = File::open(&args[1]).unwrap();
    let mut file_buffer = Vec::new();
    file.read_to_end(&mut file_buffer).unwrap();

    // Start library code!

    let debug_mode = true;

    let mut chip8_vm = Chip8::new();

    chip8_vm.boot(&file_buffer);
    // end library code!

    canvas.set_draw_color(Color::RGB(0, 255, 255));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut i = 0;
    'running: loop {
        i = (i + 1) % 255;
        canvas.set_draw_color(Color::RGB(i, 64, 255 - i));
        canvas.clear();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                _ => {}
            }
        }
        // The rest of the game loop goes here...

        // Start library code!

        if debug_mode {
            for _x in 0..(CLOCK_SPEED/FRAMES_PER_SECOND) {
                chip8_vm.step(1);
                // inspect with debugger/etc
            }
        } else {
            chip8_vm.step(CLOCK_SPEED/FRAMES_PER_SECOND);
        }
        // end library code!

        canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / FRAMES_PER_SECOND));
    }
}
